# Result

Application implemented with Spring Boot, Hibernate, H2db, Liquibase and Swagger, built with gradle 4.4.1.  
- Swagger UI: localhost:8080/swagger-ui.html   
- API Swagger Spec: localhost:8080/v2/api-docs  
- H2 Console: localhost:8080/h2-console

### Spring Profiles:
1. default - in memory database: jdbc:h2:mem:testdb
2. filedb - File backed h2 database: jdbc:h2:./warehouse
3. debugsql - SQL logging, Hibernate statistic generation 

## Task General guidelines :  

Create application in Java  
Use frameworks and libraries of your choice.  
The solution should provide approach to testing (sample tests) but extensive testing of the whole application is not required.  
Please push the solution to GitHub or BitBucket.  

##Task :

Create a simple application Warehouse. Application works with following entities:  

###Category with following attributes:  
1. Name (is unique)  
2. Parent category identification (categories form a tree)  

###Product with following attributes:  
1. Name  
2. Description  
3. Price  

The relation between Categories and Products is many-to-many. One product can belong to multiple categories. One category can contain multiple products.  

##Design the database schema and create an appropriate persistence layer in the application. Propose and implement a RESTful service that supports following functions:  

1. Add a new category  
2. Add a new product  
3. Assign/remove product from category 
4. Delete a category  
5. List category tree  
6. List products in a selected category  
