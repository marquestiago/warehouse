package the.warehouse;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import static org.junit.Assert.assertEquals;

@RequiredArgsConstructor
@Service
class TestRestClient {

    final TestRestTemplate template;

    public void post(String url, Object payload) {
        post(url, payload, Void.class);
    }

    public <C> ResponseEntity<C> post(String url, Object payload, Class<C> responseType) {
        ResponseEntity<C> response = template.
                postForEntity(url, payload, responseType);
        assertEquals("POST response incorrect status", HttpStatus.CREATED, response.getStatusCode());
        return response;
    }
}
