package the.warehouse;


import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import the.warehouse.model.Category;
import the.warehouse.repository.CategoryRepository;

import static org.hamcrest.Matchers.hasItemInArray;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoryTest {

    @Autowired
    TestRestClient testRestClient;
    private Category rootCategory;
    @Autowired
    private CategoryRepository categoryRepository;

    @Before
    public void setUp() {
        categoryRepository.deleteAll();
        this.rootCategory = new Category();
        this.rootCategory.setName(RandomStringUtils.randomAlphabetic(10));

    }

    @Test
    public void testCreateCategory() {
        ResponseEntity<Category> responseEntity = testRestClient.post("/category", rootCategory, Category.class);
        Category returned = responseEntity.getBody();
        assertNotNull(returned);
        assertEquals(rootCategory.getName(), returned.getName());
        Category[] categories = testRestClient.template.getForObject("/category", Category[].class);
        assertThat(categories, hasItemInArray(returned));
    }

    @Test
    public void testParentRemoval() {
        rootCategory = testRestClient.
                post("/category", rootCategory, Category.class).getBody();
        Category cat = new Category();
        cat.setName(RandomStringUtils.randomAlphabetic(10));
        cat.setParentId(rootCategory.getId());
        ResponseEntity<Category> response = testRestClient.
                post("/category", cat, Category.class);
        ResponseEntity<Void> responseEntity = testRestClient.template
                .exchange("/category/{id}", HttpMethod.DELETE, null,
                        Void.class, response.getBody().getId());
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

}
