package the.warehouse;


import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import the.warehouse.model.Category;
import the.warehouse.model.Product;
import the.warehouse.repository.CategoryRepository;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductCategoryTest {

    private Category category;
    private Product product;
    @Autowired
    private TestRestClient testRestClient;
    @Autowired
    private CategoryRepository categoryRepository;
    
    @Before
    public void setUp() {
        categoryRepository.deleteAll();
        category = new Category();
        category.setName(RandomStringUtils.randomAlphabetic(10));
        category = testRestClient.post("/category", category, Category.class).getBody();

        product = new Product();
        product.setName(RandomStringUtils.randomAlphabetic(10));
        product.setPrice(BigDecimal.TEN);
        product = testRestClient.post("/product", product, Product.class).getBody();
    }

    @Test
    public void testAssignCategory() {

        ResponseEntity<Void> response = testRestClient.template.postForEntity("/product/{id}/category/{categoryId}", null, Void.class,
                product.getId(), category.getId());

        assertEquals("POST to product categories incorrect status",
                HttpStatus.NO_CONTENT, response.getStatusCode());

        Product returned = testRestClient.template.getForObject("/product/{id}",
                Product.class, product.getId());

        assertThat(returned.getCategories(), hasItem(category));
    }

    @Test
    public void testUnassignCategory() {
        ResponseEntity<Void> response = testRestClient.template.postForEntity("/product/{id}/category/{categoryId}", null, Void.class,
                product.getId(), category.getId());
        assertEquals("POST to product categories incorrect status",
                HttpStatus.NO_CONTENT, response.getStatusCode());
        ResponseEntity<Void> responseEntity = testRestClient.template
                .exchange("/product/{id}/category/{categoryId}", HttpMethod.DELETE, null, Void.class, product.getId(), category.getId());
        assertEquals("DELETE to product categories incorrect status",
                HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        Product returned = testRestClient.template.getForObject("/product/{id}",
                Product.class, product.getId());

        assertThat(returned.getCategories(), not(hasItem(category)));
    }

}