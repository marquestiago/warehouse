package the.warehouse;


import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import the.warehouse.model.Product;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductTest {

    @Autowired
    private TestRestClient testRestClient;
    private Product product;

    @Before
    public void setUp() {
        this.product = new Product();
        this.product.setName(RandomStringUtils.randomAlphabetic(10));
        this.product.setPrice(BigDecimal.TEN);
    }

    @Test
    public void testCreateProduct() {
        ResponseEntity<Product> responseEntity = testRestClient.post("/product", product, Product.class);
        Product returned = responseEntity.getBody();
        assertNotNull(returned);
        assertNotNull(returned.getId());
        assertEquals(returned.getName(), returned.getName());
        Product[] products = testRestClient.template.getForObject("/product", Product[].class);
        assertThat(products, hasItemInArray(returned));
    }

    @Test
    public void testRemoval() {
        product = testRestClient.post("/product", product, Product.class).getBody();
        ResponseEntity<Void> responseEntity = testRestClient.template
                .exchange("/product/{id}", HttpMethod.DELETE, null, Void.class, product.getId());
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());

        Product[] products = testRestClient.template.getForObject("/product", Product[].class);
        assertThat(products, not(hasItemInArray(product)));
    }

}
