package the.warehouse.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import the.warehouse.model.Category;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Optional<Category> findById(Long id);

    @Query("SELECT COUNT(c) FROM Category c WHERE c.parentId IS NULL")
    Long countRoots();

}
