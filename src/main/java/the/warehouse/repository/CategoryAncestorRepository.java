package the.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import the.warehouse.model.CategoryAncestor;

import java.util.Set;

public interface CategoryAncestorRepository extends JpaRepository<CategoryAncestor, Long> {

    @Query("SELECT a.id.id FROM CategoryAncestor a where a.id.ancestorId = :id")
    Set<Long> findAncestorIds(@Param("id") Long id);
}
