package the.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import the.warehouse.model.Product;

import java.util.Optional;
import java.util.Set;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Set<Product> findByCategoriesIdIn(Set<Long> categoryIds);

    Optional<Product> findById(Long id);
}
