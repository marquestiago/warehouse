package the.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import the.warehouse.model.CategoryWithParent;

public interface CategoryWithParentRepository extends JpaRepository<CategoryWithParent, Long> {
}
