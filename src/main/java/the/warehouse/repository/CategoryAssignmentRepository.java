package the.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import the.warehouse.model.CategoryAssignment;

public interface CategoryAssignmentRepository extends JpaRepository<CategoryAssignment, CategoryAssignment.Id> {

}
