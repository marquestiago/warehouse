package the.warehouse.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import the.warehouse.model.Category;
import the.warehouse.model.CategoryNode;
import the.warehouse.model.Product;
import the.warehouse.repository.CategoryRepository;
import the.warehouse.service.CategoryCreationService;
import the.warehouse.service.CategoryProductsService;
import the.warehouse.service.CategoryTreeService;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Set;

@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/category")
public class CategoryController {

    private final CategoryRepository categoryRepository;
    private final CategoryTreeService treeService;
    private final CategoryCreationService creationService;
    private final CategoryProductsService categoryProductsService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Category create(@RequestBody Category category) {
        category.setId(null);
        return creationService.create(category);
    }

    @GetMapping
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @GetMapping("/tree")
    public CategoryNode generateTree() {
        return treeService.generateCategoryTree().orElseThrow(NoResultException::new);
    }

    @GetMapping("/{id}/product")
    public Set<Product> getProductsInCategory(@PathVariable Long id) {
        return categoryProductsService.findProductsInCategory(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        categoryRepository.delete(id);
    }

}
