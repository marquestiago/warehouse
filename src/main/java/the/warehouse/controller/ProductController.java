package the.warehouse.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import the.warehouse.model.Category;
import the.warehouse.model.CategoryAssignment;
import the.warehouse.model.Product;
import the.warehouse.repository.CategoryAssignmentRepository;
import the.warehouse.repository.CategoryRepository;
import the.warehouse.repository.ProductRepository;

import javax.persistence.NoResultException;
import javax.validation.Valid;
import java.util.List;

@Validated
@AllArgsConstructor
@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final CategoryAssignmentRepository categoryAssignmentRepository;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Product create(@RequestBody @Valid Product product) {
        product.setId(null);
        return productRepository.save(product);
    }

    @GetMapping
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @GetMapping("/{id}")
    public Product getById(@PathVariable Long id) {
        return productRepository.findOne(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        productRepository.delete(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("/{id}/category/{categoryId}")
    public void assignCategory(@PathVariable Long id, @PathVariable Long categoryId) {
        Product product = productRepository.findById(id).orElseThrow(() -> new NoResultException("Product not found"));
        Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new NoResultException("Product not found"));
        categoryAssignmentRepository.save(
                new CategoryAssignment(product.getId(), category.getId()));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}/category/{categoryId}")
    public void removeFromCategory(@PathVariable Long id, @PathVariable Long categoryId) {
        Product product = productRepository.findById(id).orElseThrow(() -> new NoResultException("Product not found"));
        Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new NoResultException("Product not found"));
        categoryAssignmentRepository.delete(new CategoryAssignment.Id(product.getId(), category.getId()));
    }
}
