package the.warehouse.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import the.warehouse.model.Category;
import the.warehouse.model.CategoryAncestor;
import the.warehouse.model.CategoryWithParent;
import the.warehouse.repository.CategoryAncestorRepository;
import the.warehouse.repository.CategoryRepository;
import the.warehouse.repository.CategoryWithParentRepository;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryCreationService {

    private final CategoryRepository categoryRepository;
    private final CategoryWithParentRepository categoryWithParentRepository;
    private final CategoryAncestorRepository ancestorRepository;

    @Transactional
    public Category create(Category category) {
        if(category.getParentId() == null) {
            return createRoot(category);
        } else {
            return createCategory(category);
        }
    }

    private Category createCategory(Category category) {
        category = categoryRepository.save(category);
        CategoryWithParent newCategory = categoryWithParentRepository.findOne(category.getId());
        storeAncestors(newCategory);
        return category;
    }

    private Category createRoot(Category category) {
        if(categoryRepository.countRoots() > 0) {
            throw new IllegalArgumentException("Root already exists");
        }
        return categoryRepository.save(category);
    }

    private void storeAncestors(CategoryWithParent newCategory) {
        CategoryWithParent parent = newCategory.getParent();
        List<CategoryAncestor> ancestors = new LinkedList<>();
        do {
            ancestors.add(new CategoryAncestor(newCategory.getId(), parent.getId()));
            parent = parent.getParent();
        } while (parent != null);
        ancestorRepository.save(ancestors);
    }
}
