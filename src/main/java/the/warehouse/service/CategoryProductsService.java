package the.warehouse.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import the.warehouse.model.Category;
import the.warehouse.model.Product;
import the.warehouse.repository.CategoryAncestorRepository;
import the.warehouse.repository.CategoryRepository;
import the.warehouse.repository.ProductRepository;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.Set;


@Service
@RequiredArgsConstructor
public class CategoryProductsService {

    private final CategoryRepository categoryRepository;
    private final CategoryAncestorRepository categoryAncestorRepository;
    private final ProductRepository productRepository;

    @Transactional
    public Set<Product> findProductsInCategory(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new NoResultException("Product not found"));
        Set<Long> subCategories = categoryAncestorRepository.findAncestorIds(category.getId());
        subCategories.add(category.getId());
        return productRepository.findByCategoriesIdIn(subCategories);
    }
}
