package the.warehouse.service;

import com.google.common.collect.HashMultimap;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import the.warehouse.model.Category;
import the.warehouse.model.CategoryNode;
import the.warehouse.repository.CategoryRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;

@Service
@RequiredArgsConstructor
public class CategoryTreeService {

    private static final Collector<Pair<Long, CategoryNode>,
            HashMultimap<Long, CategoryNode>, HashMultimap<Long, CategoryNode>> COLLECTOR =
            Collector.of(
                    HashMultimap::create,
                    (map, pair) -> map.put(pair.getLeft(), pair.getRight()),
                    (map1, map2) -> {
                        map1.putAll(map2);
                        return map1;
                    });

    private final CategoryRepository categoryRepository;

    public Optional<CategoryNode> generateCategoryTree() {
        List<Category> categories = categoryRepository.findAll();
        if (CollectionUtils.isEmpty(categories)) {
            return Optional.empty();
        } else {
            return Optional.of(buildCategoryTree(categories));
        }

    }


    CategoryNode buildCategoryTree(List<Category> categories) {
        HashMultimap<Long, CategoryNode> categoryByParent = categories.stream()
                .map(c -> ImmutablePair.of(c.getParentId(), new CategoryNode(c)))
                .collect(COLLECTOR);
        Set<CategoryNode> roots = categoryByParent.get(null);

        if (CollectionUtils.isEmpty(roots)) throw new IllegalStateException("No root category found");
        else if (roots.size() != 1) throw new IllegalStateException("Multiple root categories found");

        new HashSet<>(categoryByParent.values()).forEach(
                c -> {
                    if (categoryByParent.containsKey(c.getId())) {
                        c.getChildren().addAll(categoryByParent.get(c.getId()));
                    }
                }
        );
        return roots.iterator().next();
    }
}
