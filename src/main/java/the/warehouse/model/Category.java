package the.warehouse.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Getter
@Setter
@Entity
@Table(name = "CATEGORY")
public class Category extends CategoryBase {

    @Column(name = "PARENT_ID")
    private Long parentId;

}
