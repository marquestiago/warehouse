package the.warehouse.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Set;
import java.util.TreeSet;

@Getter
@EqualsAndHashCode
public class CategoryNode {

    public CategoryNode(Category category) {
        this.id = category.getId();
        this.name = category.getName();
    }

    private final Long id;
    private final String name;
    private final Set<CategoryNode> children = new TreeSet<>((o1, o2) -> {
        if (o1 == null && o2 == null) return 0;
        else if (o1 == null) return 1;
        else if (o2 == null) return -1;
        else return ObjectUtils.compare(o1.getId(), o2.getId());
    });

}
