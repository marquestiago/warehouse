package the.warehouse.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT_TO_CATEGORY")
public class CategoryAssignment {

    public CategoryAssignment(Long productId, Long categoryId) {
        this.id = new Id(productId, categoryId);
    }

    @EmbeddedId
    private Id id;

    @Embeddable
    @EqualsAndHashCode
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    public static class Id implements Serializable {

        @Column(name = "PRODUCT_ID")
        private Long productId;

        @Column(name = "CATEGORY_ID")
        private Long categoryId;
    }
}
