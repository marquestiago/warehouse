package the.warehouse.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "CATEGORY_TO_ANCESTOR")
public class CategoryAncestor {

    public CategoryAncestor(Long id, Long ancestorId) {
        this.id = new Id(id, ancestorId);
    }

    @EmbeddedId
    private Id id;

    @Embeddable
    @EqualsAndHashCode
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    static class Id implements Serializable {

        @Column(name = "ID")
        private Long id;

        @Column(name = "ANCESTOR_ID")
        private Long ancestorId;
    }
}
