package the.warehouse.service;


import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import the.warehouse.model.Category;
import the.warehouse.model.CategoryNode;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CategoryTreeServiceTest {

    private final CategoryTreeService service = new CategoryTreeService(null);

    /**
     * root
     * - 2
     * - 3
     * - 4
     */
    @Test
    public void testBasicTree() {
        Pair<CategoryNode, List<Category>> rootAndCategories =
                new CategoryTreeBuilder().root(0L)
                        .add(2L, 0L).add(3L, 0L)
                        .add(4L, 0L).build();

        CategoryNode actual = service.buildCategoryTree(rootAndCategories.getRight());
        assertThat(actual, is(rootAndCategories.getLeft()));
        List<Long> childIds = actual.getChildren().stream().map(CategoryNode::getId).collect(Collectors.toList());
        assertThat(childIds, containsInAnyOrder(2L, 3L, 4L));
    }

    /**
     * root
     * - 2
     * -- 5
     * -- 9
     * - 3
     * - 7
     * -- 6
     * -- 4
     * - 8
     */
    @Test
    public void test3LevelTree() {

        Pair<CategoryNode, List<Category>> rootAndCategories =
                new CategoryTreeBuilder().root(1L)
                        .add(2L, 1L).add(5L, 2L).add(9L, 2L)
                        .add(3L, 1L)
                        .add(7L, 1L).add(6L, 7L).add(4L, 7L)
                        .add(8L, 1L).build();

        CategoryNode actual = service.buildCategoryTree(rootAndCategories.getRight());
        assertThat("Trees don't match", actual, is(rootAndCategories.getLeft()));
    }

    private class CategoryTreeBuilder {

        private final List<Category> categories = new LinkedList<>();
        private final Map<Long, CategoryNode> nodeByIdMap = new HashMap<>();

        private CategoryNode root;

        CategoryTreeBuilder root(Long id) {
            assertNull("root already set", root);
            root = new CategoryNode(addCategory(id, null));
            nodeByIdMap.put(id, root);
            return this;
        }

        CategoryTreeBuilder add(Long id, Long parentId) {
            assertNotNull("root not set", root);
            assertTrue("parent not found: " + parentId, nodeByIdMap.containsKey(parentId));
            assertNotNull("Null id not allowed", id);
            addNode(addCategory(id, parentId));
            return this;
        }

        Pair<CategoryNode, List<Category>> build() {

            Pair<CategoryNode, List<Category>> built =
                    ImmutablePair.of(root, new LinkedList<>(categories));
            root = null;
            categories.clear();
            nodeByIdMap.clear();
            return built;
        }

        private void addNode(Category category) {
            CategoryNode node = new CategoryNode(category);
            nodeByIdMap.put(node.getId(), node);
            nodeByIdMap.get(category.getParentId()).getChildren()
                    .add(node);
        }

        private Category addCategory(long id, Long parentId) {
            Category category = new Category();
            category.setId(id);
            category.setName(RandomStringUtils.randomAlphabetic(10));
            category.setParentId(parentId);
            categories.add(category);
            return category;
        }
    }

}
